#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mem.h"

void debug(const char *fmt, ...);

void test_allocate() {
  debug("test_allocate...\n");

  void *heap = heap_init(0);

  void *c1 = _malloc(sizeof(uint64_t));
  void *c2 = _malloc(sizeof(uint64_t));
  void *c3 = _malloc(sizeof(uint64_t));

  debug_heap(stdout, heap);

  _free(c1);
  _free(c2);
  _free(c3);

  debug_heap(stdout, heap);

  heap_term();

  debug("\n");
}

void test_free_one() {
  debug("test_free_one...\n");

  void *heap = heap_init(0);

  _malloc(sizeof(uint64_t));
  void *c2 = _malloc(sizeof(uint64_t));
  _malloc(sizeof(uint64_t));

  debug_heap(stdout, heap);

  _free(c2);

  debug_heap(stdout, heap);

  heap_term();

  debug("\n");
}

void test_free_two() {
  debug("test_free_two...\n");

  void *heap = heap_init(0);

  _malloc(sizeof(uint64_t));
  void *c2 = _malloc(sizeof(uint64_t));
  void *c3 = _malloc(sizeof(uint64_t));

  debug_heap(stdout, heap);

  _free(c3);
  _free(c2);

  debug_heap(stdout, heap);

  heap_term();

  debug("\n");
}

void test_heap_grow() {
  debug("test_heap_grow...\n");

  void *heap = heap_init(0);

  void *obstacle_addr = heap + 0x2000;
  void *obstacle = mmap(obstacle_addr, 4096, PROT_READ | PROT_WRITE,
                        MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  printf("heap: %p\n", heap);
  printf("obstacle: %p\n", obstacle);

  void *c1 = _malloc(4096);
  debug_heap(stdout, heap);
  void *c2 = _malloc(4096);
  debug_heap(stdout, heap);
  void *c3 = _malloc(4096);
  debug_heap(stdout, heap);

  _free(c3);
  debug_heap(stdout, heap);
  _free(c2);
  debug_heap(stdout, heap);
  _free(c1);
  debug_heap(stdout, heap);

  heap_term();

  munmap(obstacle, 4096);

  debug("\n");
}

int main() {
  test_allocate();
  test_free_one();
  test_free_two();
  test_heap_grow();
}
