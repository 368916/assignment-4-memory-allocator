#define _DEFAULT_SOURCE

#include "mem.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);
void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) {
  return block->capacity.bytes >= query;
}
static size_t pages_count(size_t mem) {
  return mem / getpagesize() + ((mem % getpagesize()) > 0);
}
static size_t round_pages(size_t mem) {
  return getpagesize() * pages_count(mem);
}

static void block_init(void *restrict addr, block_size block_sz,
                       void *restrict next) {
  *((struct block_header *)addr) = (struct block_header){
      .next = next, .capacity = capacity_from_size(block_sz), .is_free = true};
}

static size_t region_actual_size(size_t query) {
  return size_max(round_pages(query), REGION_MIN_SIZE);
}

extern inline bool region_is_invalid(const struct region *r);

static void *map_pages(void const *addr, size_t length, int additional_flags) {
  return mmap((void *)addr, length, PROT_READ | PROT_WRITE,
              MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

static size_t get_region_size(size_t query) {
  const block_capacity query_capacity = (block_capacity){.bytes = query};
  const block_size query_requested_size = size_from_capacity(query_capacity);
  const size_t region_size = region_actual_size(query_requested_size.bytes);

  return region_size;
}

/*  аллоцировать регион памяти и инициализировать его блоком */
/* to implement */
static struct region alloc_region(void const *addr, size_t query) {
  const size_t region_size = get_region_size(query);
  void *result = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);
  if (result == MAP_FAILED)
    result = map_pages(addr, region_size, 0);
  if (result == MAP_FAILED)
    result = NULL;

  struct region region = {
      .addr = result, .size = region_size, .extends = result == addr};

  if (result) {
    block_size region_bs = (block_size){.bytes = region.size};
    block_init(result, region_bs, NULL);
  }

  return region;
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
  const struct region region = alloc_region(HEAP_START, initial);
  if (region_is_invalid(&region))
    return NULL;

  return region.addr;
}

static bool blocks_continuous(struct block_header const *fst,
                              struct block_header const *snd);

/*  освободить всю память, выделенную под кучу */
/* to implement */
void heap_term() {
  size_t region_size = 0;
  struct block_header *region_start = HEAP_START;

  struct block_header *iter = HEAP_START;
  while (iter) {
    region_size += size_from_capacity(iter->capacity).bytes;

    struct block_header *next = iter->next;
    if (!blocks_continuous(iter, next)) {
      munmap(region_start, region_size);
      region_size = 0;
      region_start = next;
    }

    iter = next;
  }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )---
 */
static bool block_splittable(struct block_header *restrict block,
                             size_t query) {
  return block->is_free &&
         query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <=
             block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
  if (!block_splittable(block, query))
    return false;

  const block_capacity curr_capacity = (block_capacity){.bytes = query};
  const block_size new_size =
      (block_size){.bytes = block->capacity.bytes - curr_capacity.bytes};

  block->capacity = curr_capacity;

  struct block_header *new = (void *)block->contents + query;
  block_init(new, new_size, block->next);

  block->next = new;

  return true;
}

/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
  return (void *)(block->contents + block->capacity.bytes);
}
static bool blocks_continuous(struct block_header const *fst,
                              struct block_header const *snd) {
  return (void *)snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst,
                      struct block_header const *restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

/* to implement */
static bool try_merge_with_next(struct block_header *block) {
  if (!block)
    return false;

  struct block_header *next = block->next;
  if (!next || !mergeable(block, next))
    return false;

  const block_size total_size =
      (block_size){.bytes = size_from_capacity(block->capacity).bytes +
                            size_from_capacity(next->capacity).bytes};
  const block_capacity total_capacity = capacity_from_size(total_size);

  block->capacity = total_capacity;
  block->next = next->next;

  return true;
}

static void try_merge_until_fail(struct block_header *block) {
  while (try_merge_with_next(block))
    ;
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum { BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED } type;
  struct block_header *block;
};

static bool is_good(struct block_header *block, size_t query) {
  return block && block->is_free && block_is_big_enough(query, block);
}

static struct block_search_result
find_good_or_last(struct block_header *restrict block, size_t sz) {
  bool found = false;

  struct block_header *last = block;
  struct block_header *iter = block;
  while (iter) {
    try_merge_until_fail(iter);

    found = is_good(iter, sz);
    if (found)
      break;

    last = iter;
    iter = iter->next;
  }

  if (!found)
    return (struct block_search_result){.type = BSR_REACHED_END_NOT_FOUND,
                                        .block = last};

  return (struct block_search_result){.type = BSR_FOUND_GOOD_BLOCK,
                                      .block = iter};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь
 расширить кучу Можно переиспользовать как только кучу расширили. */
static struct block_search_result
try_memalloc_existing(size_t query, struct block_header *block) {
  struct block_search_result result = find_good_or_last(block, query);
  if (result.type == BSR_CORRUPTED || result.type == BSR_REACHED_END_NOT_FOUND)
    return result;

  split_if_too_big(result.block, query);
  result.block->is_free = false;

  return result;
}

/* to implement */
static struct block_header *grow_heap(struct block_header *restrict last,
                                      size_t query) {
  const void *hint = block_after(last);
  const struct region region = alloc_region(hint, query);
  if (region_is_invalid(&region))
    return NULL;

  last->next = region.addr;
  if (region.extends) {
    try_merge_until_fail(last);

    if (is_good(last, query))
      return last;
  }

  return last->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока
 */
static struct block_header *memalloc(size_t query,
                                     struct block_header *heap_start) {
  const size_t actual_query = size_max(query, BLOCK_MIN_CAPACITY);
  struct block_search_result result =
      try_memalloc_existing(actual_query, heap_start);
  if (result.type == BSR_CORRUPTED)
    return NULL;

  if (result.type == BSR_FOUND_GOOD_BLOCK) {
    return result.block;
  }

  struct block_header *last = result.block;
  struct block_header *block = grow_heap(last, actual_query);
  if (!block)
    return NULL;

  result = try_memalloc_existing(actual_query, block);

  return result.block;
}

void *_malloc(size_t query) {
  const size_t size = size_max(query, BLOCK_MIN_CAPACITY);
  const size_t aligned_size = size + BLOCK_ALIGN - size % BLOCK_ALIGN;
  struct block_header *const addr =
      memalloc(aligned_size, (struct block_header *)HEAP_START);

  if (addr)
    return addr->contents;
  else
    return NULL;
}

static struct block_header *block_get_header(void *contents) {
  return (struct block_header *)(((uint8_t *)contents) -
                                 offsetof(struct block_header, contents));
}

void _free(void *mem) {
  if (!mem)
    return;
  struct block_header *header = block_get_header(mem);
  header->is_free = true;

  try_merge_until_fail(header);
}
